import React from "react";
import './dialog.css';
import Dialog from "./dialog";


class LoginDialog extends React.Component {

    state = {
        username: "",
        password: "",
    }

    async auth(event) {
        event.preventDefault();

        let resp = await fetch(`/api/auth?username=${this.state.username}&password=${this.state.password}`);
        let jsonResp = await resp.json();

        var el = document.getElementById('form-error');
        if (jsonResp['result'] === 'Ok') {
            el.textContent = "Success!"
            el.style = 'color: green';
            window.location.href = '/'
        } else {
            el.textContent = "Wrong credentials!"
            el.style = 'color: red';
        }
    }
    
    render() {
        return (
            <Dialog dialog_id="login">
                <form onSubmit={event => this.auth(event)} className='login-form'>
                    <span className='login-title'>LOGIN</span>
                    <input className="login-input" value={this.state.username} onChange={event => this.setState({username: event.target.value})} type="text" name="username" placeholder="Username"/>
                    <input className="login-input" value={this.state.password} onChange={event => this.setState({password: event.target.value})} type="password" name="password" placeholder="Password"/>
                    <p id="form-error"></p>
                    <button className="login-button">Login</button>
                </form>
            </Dialog>
        );
    }
}

export default LoginDialog;