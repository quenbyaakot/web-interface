import React from "react";
import Dialog from './dialog'
import './dialog.css'

class DescriptionDialog extends React.Component {

    state = {
        name: "Loading",
        points: "0",
        ip: "",
    }

    componentDidMount() {
        this.setState(this.props.raw_info)
    }
    
    render(props) {
        
        return (
            <Dialog closeDialog={this.props.closeDialog} noTarget>
                <form className='login-form'>
                    <span className='description-title' id="desc-title">{this.state.name}</span>
                    <span className='description-points'><span id="desc-coins">{this.state.points}</span> coins</span>
                    <span className='description-addr'>IP: <span id="desc-ip">{this.state.addr}</span></span>
                    { this.state.protected && this.state.protected.password && 
                        <span id="description-password" className='description-password'>Password: <span id="desc-password">{this.state.protected.password}</span></span>
                    }
                    { this.state.protected && this.state.protected.description &&                     
                        <span id="description-text" className='description-text'>{this.state.protected.description}</span>
                    }
                </form>
            </Dialog>
        );
    }
}

export default DescriptionDialog;