import React from "react";
import './dialog.css'

class Dialog extends React.Component {

    onClick() {
        if (this.props.closeDialog) {
           this.props.closeDialog();
        }
    }
    
    render(props) {
        let ext_props = {
            href: '#'
        }

        if(this.props.noTarget) {
            ext_props = {}
        }

        return (
            <div id={this.props.dialog_id} className={'dialog '+(this.props.noTarget?'no-target':'')}>
                <a onClick={_ => this.onClick()} className='dialog-bg' {...ext_props}/>
                <div className='dialog-container'>
                    <a onClick={_ => this.onClick()} {...ext_props}><img className='dialog-close' alt='close.svg' src={require('../../icons/close.svg')}/></a>
                    <div className='dialog-inner'>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

export default Dialog;