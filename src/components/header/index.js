import React from "react";
import './header.css'

class Header extends React.Component {
    state = {
        user: null
    }

    componentDidMount() {
        this.fetchUser()
    }

    async fetchUser() {
        let resp = await fetch(`/api/info/`);
        let jsonResp = await resp.json();

        if(jsonResp.role != 'Guest') {
            this.setState({user: jsonResp})
        }
    }

    render() {
        return (
            <div id='header'>
                <nav className='navigation-bar'>
                    <div className='navigation-bar-logo'>
                    </div>
                    <ul className='navigation-bar-links'>
                        <li className='navigation-bar-item'>
                            <a className='navigation-bar-link' href='/challenges'>Challenges</a>
                        </li>
                        <li className='navigation-bar-item'>
                            <a className='navigation-bar-link' href='/scoreboard'>Scoreboard</a>
                        </li>
                        <li className='navigation-bar-item'>
                            <a className='navigation-bar-link' href='/history'>History</a>
                        </li>
                    </ul>
                    <div className='navigation-bar-account'>
                        { /*<a className='navigation-bar-account-nickname-link' href='/profile'>AndarGuy</a>
                            <a className='navigation-bar-account-account-settings-link' href='/settings'>
                                <img src={require('../../icons/menu-down.svg')} className='navigation-bar-account-account-settings-image navigation-bar-image' alt='settings_icon'/></a>*/ }
                        
                        {this.state.user? 
                            <a className='navigation-bar-account-nickname-link' style={{marginRight: '2vw'}}>{this.state.user.name}</a>:
                            <a className='navigation-bar-account-nickname-link' href='#login' style={{marginRight: '2vw'}}>Login</a>
                        }
                    </div>
                </nav>
            </div>
        );
    }
}

export default Header;