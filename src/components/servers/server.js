import React from "react";
import Timer from "./timer";
import './servers.css';

const timeToHack = 300000;

class Server extends React.Component {

    render(props) {
        return (
            <div className='server-container'>
                <div className='server-align'>
                <img className='server-image' alt={this.props.image} src={this.props.image}></img>
                    <div className='server-text-container'>
                        <a onClick={_ => this.props.showDescription(this.props.info)} className='server-name'>{this.props.name}</a>
                        <div className='server-points'>{`${this.props.points} coins`}</div>
                    </div>
                    {this.props.status === 'disconnected' ? <Timer startTime={this.props.startHackingTime} duration={timeToHack}/> : <div className='server-status' status={this.props.status}></div>}
                </div>
            </div>
        );
    }
}

export default Server;