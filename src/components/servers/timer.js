import React from "react";

class Timer extends React.Component {

    state = {};

    componentDidMount() {
        this.doTimer();
        let minutesLeft = 0;
        let secondsLeft = 0;
        this.setState({
            minutesLeft, secondsLeft
        });
    }
    

    doTimer = async () => {
        let startTime = this.props.startTime;
        let currentTime = Date.now();
        let endTime = startTime + this.props.duration;
        let timeLeft = endTime - currentTime;
        if (timeLeft < 0) {
            return;
        };
        
        this.setState({
            minutesLeft: Math.floor(timeLeft / 1000 / 60),
            secondsLeft: Math.floor(timeLeft / 1000 % 60)
        });

        setTimeout(this.doTimer, 1000);
    }

    render(props) {
        return (
            <div className='timer'>
            {`${this.state.minutesLeft}:${this.state.secondsLeft < 10 ? `0${this.state.secondsLeft}`: this.state.secondsLeft}`}
            </div>
        );
    }
}

export default Timer;