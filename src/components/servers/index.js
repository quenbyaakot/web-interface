import React from "react";
import Server from "./server"

class Servers extends React.Component {
    state = {
        servers: {},
        info: {}
    }

    ws = new WebSocket('ws://localhost:3333')

    componentDidMount() {
        this.ws.onmessage = evt => {
            const message = JSON.parse(evt.data)
            this.setState({servers: message});
        }
    }

    async fetchInfo(ip) {
        let resp = await fetch(`/api/task/${ip}`);
        let jsonResp = await resp.json();

        let info = jsonResp['result']['Ok'] || {name: 'Error'}

        this.setState({info: {
            [ip]: info
        }})
    }

    render() {
        return (
            <div id='servers'>
                {this.state.servers && Object.keys(this.state.servers).map((ip, key) => {
                    if (this.state.info[ip] == null) {
                        this.fetchInfo(ip)
                    }

                    let info = this.state.info[ip] || {};

                    let status = "unavailable";

                    var server = {
                        addr: info.addr || "",
                        name: info.name || "Loading",
                        points: info.points || "0",
                        image: info.icon || "/imgs/treasure.svg",
                        status: status,
                    }
                    
                    if (typeof(this.state.servers[ip]) == 'object') {
                        status = Object.keys(this.state.servers[ip])[0].toLowerCase();

                        if(status === "disconnected") {
                            server.startHackingTime = this.state.servers[ip]["Disconnected"]
                        }

                        server.status = status
                    }

                    return (
                        <Server showDescription={this.props.showDescription} key={key} info={info} name={server.name} points={server.points} status={server.status} image={server.image} startHackingTime={server.status === 'disconnected' ? server.startHackingTime:-1}/>
                    )
                })}
            </div>
        );
    }
}

export default Servers;