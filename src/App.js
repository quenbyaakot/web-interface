import React from "react";
import Header from "./components/header/index";
import Servers from "./components/servers/index";
import Login from "./components/dialog/login";
import Description from "./components/dialog/description";

class App extends React.Component {

  state = {
    info: null
  }

  closeDialog() {
    this.setState({info: null})
  }

  showDescription(info) {
    this.setState({info});
  }

  render() {
    return (
      <div id='container'>
        {this.state.info && <Description closeDialog={this.closeDialog.bind(this)} raw_info={this.state.info}/>}
        <Login />
        <Header />
        <div id='page-content'>
          <Servers showDescription={this.showDescription.bind(this)}/>
        </div>
      </div>
    );
  }
}
export default App;