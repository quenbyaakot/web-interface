# This repository contains the source code of `Quenbyaakot` **CastleWars** web interface. 

This app powered by React that gives an ability to instantly update the interface when the server state changed. Also, we use webhook's to fetch server updates.